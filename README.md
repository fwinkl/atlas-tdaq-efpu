# Phase-II Event Filter container registry

This repository serves as the container registry for the Phase-II Event Filter applications (EFPU).

## athenahlt
Container to run athenaHLT in Kubernetes for testing/evaluation purposes. The container is built on top of a standard el9 base image and only adds a few scripts. The actual offline release needs to be available on the host (e.g. through cvmfs). A predefined data file is used which is processed in an infinite loop.

#### Running the container
```sh
docker run --shm-size=64G -v /cvmfs:cvmfs gitlab-registry.cern.ch/fwinkl/atlas-tdaq-efpu/athenahlt:latest
```
If the release is available elsewhere replace the `/cvmfs` mount with a suitable directory (e.g. `/sw` at Point-1).

Note the increased shared memory segment size, which is needed by athenaHLT/HLTMPPU.

#### Manually building the container
This is usually done by the CI but for local testing use:
```sh
cd athenahlt
docker build -v /cvmfs:/cvmfs -t athenahlt .
```
