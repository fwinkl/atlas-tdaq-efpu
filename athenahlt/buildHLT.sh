#!/usr/bin/env bash
# Build EF container

release=$1

# asetup
source setup_env.sh
asetup ${release}
ExtraArgs=$2

# Create config JSON
set -x
athenaHLT.py --dump-config-exit TriggerJobOpts.runHLT Trigger.triggerMenuSetup="PhysicsP1_pp_run3_v1_HLTReprocessing_prescale" Trigger.doLVL1=True $ExtraArgs

# We don't need these files in the image
rm -f HLTJobOptions.pkl HLTJobOptions.db.json
