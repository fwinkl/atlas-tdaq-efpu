# Setup asetup for cvmfs and Point-1

shopt -s expand_aliases
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

# if running from cvmfs use ALRB
if [ -d "${ATLAS_LOCAL_ROOT_BASE}" ]; then
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
# otherwise use $AtlasSetup
else
    unset ATLAS_LOCAL_ROOT_BASE
    if [ -z "${AtlasSetup}" ]; then  # assume Point-1
        export AtlasSetup=/sw/atlas/AtlasSetup
    fi
    alias asetup='source ${AtlasSetup}/scripts/asetup.sh'
fi
