#!/usr/bin/env bash
# Usage: runHLT.sh [ARGS]

# To get a semi-clean shutdown on SIGTERM (i.e. docker stop), catch the signal
# and forward it to the athenaHLT children. The mother process will then do a stop/finalize.
_term() {
  echo "Caught SIGTERM signal. Forwarding to athenaHLT children..."
  for p in `ps -o pid= --ppid $pid`; do
      kill -SIGTERM $p
  done
}

trap _term SIGTERM

# asetup the same release as was used during the build of the container
source setup_env.sh
asetup

# launch athenaHLT
set -x
athenaHLT.py --imf --loop-files --threads=${EFPU_NTHREADS} --file=${EFPU_INPUT} $@ HLTJobOptions.json &
pid=$!
wait $pid

# blah
